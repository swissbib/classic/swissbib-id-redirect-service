package api

import (
	"context"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func EstablishMongoDB(host, port, caCertificatePath, clientCombinedTlsPath,
	username, password, database, collection string, logger *ublogger.Logger) (*mongo.Collection, *mongo.Client, error) {
	connectionString := "mongodb://" + host + ":" + port + "/?tls=true&tlsCAFile=" + caCertificatePath + "&tlsCertificateKeyFile=" + clientCombinedTlsPath
	logger.Info().Msgf("Connecting to MongoDB: %s", connectionString)
	clientOptions := options.Client().
		SetAppName("ConcordanceService").
		SetAuth(options.Credential{
			Username:   username,
			Password:   password,
			AuthSource: database,
		}).
		ApplyURI(connectionString)

	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		return nil, nil, err
	}

	databaseClient := client.Database(database)
	collectionClient := databaseClient.Collection(collection)

	err = client.Ping(context.Background(), nil)
	if err != nil {
		return nil, nil, err
	}

	return collectionClient, client, nil
}
