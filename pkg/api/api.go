package api

import (
	"context"
	"emperror.dev/errors"
	"github.com/gin-gonic/gin"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/http2"
	"net/http"
	"regexp"
)

func EstablishServiceController(address string, collection *mongo.Collection, logger ublogger.Logger) (*Controller, error) {
	r := gin.New()
	r.Use(DefaultStructuredLogger(&logger))
	r.Use(gin.Recovery())
	return &Controller{
		router:          r,
		collection:      collection,
		internalAddress: address,
		idsBBRegex:      regexp.MustCompile("(IDSBB)(.*)"),
		logger:          logger,
	}, nil
}

type Controller struct {
	router          *gin.Engine
	server          http.Server
	collection      *mongo.Collection
	internalAddress string
	idsBBRegex      *regexp.Regexp
	logger          ublogger.Logger
}

func (ctrl *Controller) init() error {
	ctrl.router.GET("/Record/:swissbibId", ctrl.redirectRecordPage)
	ctrl.router.GET("/Search/Results", ctrl.redirectToSwisscoveryWithSearch)
	ctrl.router.NoRoute(ctrl.redirectToSwisscovery)

	ctrl.server = http.Server{
		Addr:    ctrl.internalAddress,
		Handler: ctrl.router,
	}

	if err := http2.ConfigureServer(&ctrl.server, nil); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (ctrl *Controller) Start() {
	err := ctrl.init()
	if err != nil {
		ctrl.logger.Fatal().Msgf("cannot initialize server: %v", err)
		return
	}
	go func() {
		ctrl.logger.Info().Msgf("starting server at http://%s\n", ctrl.server.Addr)
		if err := ctrl.server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			ctrl.logger.Fatal().Msgf("server on '%s' ended: %v", ctrl.server.Addr, err)
		}
	}()
}

func (ctrl *Controller) Stop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

func (ctrl *Controller) GracefulStop() {
	err := ctrl.server.Shutdown(context.Background())
	if err != nil {
		return
	}
}

func (ctrl *Controller) redirectRecordPage(c *gin.Context) {
	swissbibId := c.Param("swissbibId")

	target, err := ctrl.findTargetByID(swissbibId, c.Request.URL.String())
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			ctrl.logger.Error().Interface("id", swissbibId).Msgf("record with id %s not found", swissbibId)
		}
		c.Redirect(301, "https://swisscovery.slsp.ch/")
		return
	}
	c.Redirect(301, target)
}

func (ctrl *Controller) findTargetByID(swissbibID, request string) (string, error) {
	filter := bson.M{"_id": swissbibID}
	var result bson.M
	err := ctrl.collection.FindOne(context.Background(), filter).Decode(&result)
	if err != nil {
		ctrl.logger.Error().Err(err).Str("id", swissbibID).Str("request", request).Msg("cannot find record")
		return "", err
	}

	target, ok := result["target"].(string)
	if !ok {
		return "", nil
	}
	ctrl.logger.Info().Interface("id", swissbibID).Interface("target", target).Msg("found record")
	return target, nil
}

func (ctrl *Controller) redirectToSwisscoveryWithSearch(c *gin.Context) {
	queryType, typeExists := c.GetQuery("type")
	lookFor, lookForExists := c.GetQuery("lookfor")
	if !typeExists || !lookForExists {
		c.Redirect(301, "https://swisscovery.slsp.ch")
		return
	}
	matchedId := ctrl.idsBBRegex.FindStringSubmatch(lookFor)
	if queryType == "ctrlnum" && matchedId != nil {
		if len(matchedId) > 1 {
			redirectURL := "https://swisscovery.slsp.ch/discovery/search?query=lds02,contains,(IDSBB)" + matchedId[2] + "DSV01&tab=41SLSP_DN_CI&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION&offset=0"
			c.Redirect(301, redirectURL)
			return
		}
	}

	c.Redirect(301, "https://swisscovery.slsp.ch")
}

func (ctrl *Controller) redirectToSwisscovery(c *gin.Context) {
	c.Redirect(301, "https://swisscovery.slsp.ch")
}
