package api

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
	"os"
)

func LoadLogger(logToLogstash bool,
	caFile, certFile, keyFile, level, file, host, namespace, dataset, logstashLevel string, port int) (*ublogger.Logger, *ublogger.NewStash, error) {
	if logToLogstash {
		logstashTlsConfig, logstashErr := loadLogstashCert(caFile, certFile, keyFile)
		if logstashErr != nil {
			return nil, nil, fmt.Errorf("cannot load logstash tls certificate: %v", logstashErr)
		}
		logstashLogger, logstash, _ := ublogger.CreateUbMultiLoggerTLS(level, file,
			ublogger.SetLogStash(host, port, namespace, logstashLevel),
			ublogger.SetDataset(dataset),
			ublogger.SetTLS(true),
			ublogger.SetTLSConfig(logstashTlsConfig),
			ublogger.SetSkipVerify(false),
		)
		return logstashLogger, logstash, nil
	} else {
		consoleLogger, _, _ := ublogger.CreateUbMultiLogger("", 0, level, "", file, namespace)
		return consoleLogger, nil, nil
	}
}

func loadLogstashCert(tlsCaCertPath, tlsCertPath, tlsKeyPath string) (*tls.Config, error) {
	if tlsCertPath == "" || tlsKeyPath == "" {
		return nil, nil
	}
	certificate, err := tls.LoadX509KeyPair(tlsCertPath, tlsKeyPath)
	if err != nil {
		return nil, err
	}

	caCert, err := os.ReadFile(tlsCaCertPath)
	if err != nil {
		return nil, err
	}

	caCertPool, err := x509.SystemCertPool()
	if err != nil {
		caCertPool = x509.NewCertPool()
	}
	if caCert != nil {
		caCertPool.AppendCertsFromPEM(caCert)
	}
	return &tls.Config{
		Certificates: []tls.Certificate{certificate},
		RootCAs:      caCertPool,
	}, nil
}
