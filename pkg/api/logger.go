package api

import (
	"github.com/gin-gonic/gin"
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
	"time"
)

// DefaultStructuredLogger logs a gin HTTP request in JSON format.
func DefaultStructuredLogger(logger *ublogger.Logger) gin.HandlerFunc {
	return StructuredLogger(logger)
}

// StructuredLogger logs a gin HTTP request in JSON format. Allows to set the
// logger for testing purposes.
func StructuredLogger(logger *ublogger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

		start := time.Now() // Start timer
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		// Process request
		c.Next()

		// Fill the params
		param := gin.LogFormatterParams{}

		param.TimeStamp = time.Now() // Stop timer
		param.Latency = param.TimeStamp.Sub(start)
		if param.Latency > time.Minute {
			param.Latency = param.Latency.Truncate(time.Second)
		}

		param.Method = c.Request.Method
		param.StatusCode = c.Writer.Status()
		param.ErrorMessage = c.Errors.ByType(gin.ErrorTypePrivate).String()
		param.BodySize = c.Writer.Size()
		if raw != "" {
			path = path + "?" + raw
		}
		param.Path = path

		// Get request headers
		useragent := c.Request.UserAgent()

		// Log using the params
		if c.Writer.Status() >= 500 {
			logger.Error().
				Str("method", param.Method).
				Int("status_code", param.StatusCode).
				Int("body_size", param.BodySize).
				Str("path", param.Path).
				Str("latency", param.Latency.String()).
				Str("useragent", useragent).
				Msg(param.ErrorMessage)
		} else {
			logger.Info().
				Str("method", param.Method).
				Int("status_code", param.StatusCode).
				Int("body_size", param.BodySize).
				Str("path", param.Path).
				Str("latency", param.Latency.String()).
				Str("useragent", useragent).
				Msg("successful request")
		}
	}
}
