# https://hub.docker.com/_/golang/
FROM golang:1.23.0 as builder

ENV GO111MODULE=on
ENV GOPRIVATE=gitlab.switch.ch/ub-unibas/*
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64


WORKDIR /source
COPY . /source
RUN go mod download
WORKDIR /source/cmd/swissbib-api
RUN go build -o /app/swissbib-api

FROM alpine:latest
WORKDIR /
COPY --from=builder /app /app

ENTRYPOINT ["/app/swissbib-api"]