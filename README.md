# Legacy Swissbib Id Redirect Service
This service accepts any requests to the swissbib domains and redirects them to swisscovery.
Swissbib record pages are mapped via the id parameter to a search query in swisscovery. The mapping is achieved
with a concordance table stored in mongodb.

## Configuration
All configuration is done via environment variables. Check [config.go](/cmd/swissbib-api/config.go) for the options.

## Testing
A few example requests that should be resolved correctly.

### SSH Tunnel

```shell
ssh -L 29017:ub-mongodb.ub.unibas.ch:29017 root@ub-mongodb.ub.unibas.ch
```

```shell
ssh -L 5046:ub-log.ub.unibas.ch:5046 root@ub-log.ub.unibas.ch
```

### Example Search (IDSBB006000115)

Original request:
```http request
GET https://baselbern.swissbib.ch/Search/Results?type=ctrlnum&lookfor=IDSBB006000115
```

Local request:
```http request
GET http://localhost:5000/Search/Results?type=ctrlnum&lookfor=IDSBB006000115
```

Target redirect:
```http request
GET https://swisscovery.slsp.ch/discovery/search?query=lds02,contains,(IDSBB)006000115DSV01&tab=41SLSP_DN_CI&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION&offset=0
```


### Example Record (126360510)
Original request:
```http request
GET https://baselbern.swissbib.ch/Record/126360510
```

Local request:
```http request
GET http://localhost:5000/Record/126360510
```

Target redirect:
```http request
GET https://swisscovery.slsp.ch/discovery/search?query=lds02,contains,(NEBIS)000031411EBI01&tab=41SLSP_NETWORK&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION
```