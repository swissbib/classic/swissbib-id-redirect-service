package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"os"
	"os/signal"
	"swissbib-id-redirect-service/pkg/api"
	"syscall"
)

func main() {
	config, err := LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load configuration: %v", err)
		return
	}

	logger, logstash, err := api.LoadLogger(
		config.LogToLogstash,
		config.LogstashCertCa,
		config.LogstashCert,
		config.LogstashCertKey,
		config.LogLevel,
		config.LogFile,
		config.LogstashHost,
		config.LogstashNamespace,
		config.LogstashDataset,
		config.LogstashLogLevel,
		config.LogstashPort)
	if err != nil {
		logger.Fatal().Msgf("cannot create logger: %v", err)
		return
	}
	if config.LogToLogstash && logstash != nil {
		defer logstash.Close()
	}

	config.DisplayConfigLog(logger)

	logger.Info().Msg("Starting Certificate API")

	collection, mongodbClient, err := api.EstablishMongoDB(config.MongoDbHost, config.MongoDbPort,
		config.MongoDbTlsCaCertPath, config.MongoDbTlsCombinedCertificate,
		config.MongoDbUsername, config.MongoDbPassword, config.MongoDbDatabase, config.MongoDbCollection, logger)
	if err != nil {
		log.Fatalf("Cannot establish MongoDB connection: %v", err)
		return
	}
	defer func(mongodbClient *mongo.Client, ctx context.Context) {
		errDisconnect := mongodbClient.Disconnect(ctx)
		if errDisconnect != nil {
			log.Fatalf("Cannot disconnect from MongoDB: %v", err)
			return
		}
	}(mongodbClient, context.Background())

	gin.DisableConsoleColor()
	gin.SetMode(gin.ReleaseMode)

	controller, err := api.EstablishServiceController(config.HttpAddress, collection, *logger)
	if err != nil {
		log.Fatalf("Cannot establish service controller: %v", err)
		return
	}
	defer controller.GracefulStop()
	controller.Start()
	logger.Info().Msgf("Service started.")
	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	logger.Info().Msg("Press ctrl+c to stop server.")
	s := <-done
	logger.Info().Msg("Got signal: " + s.String())
}

func LoadLogstashCert(tlsCaCertPath, tlsCertPath, tlsKeyPath string) (*tls.Config, error) {
	if tlsCertPath == "" || tlsKeyPath == "" {
		return nil, nil
	}
	certificate, err := tls.LoadX509KeyPair(tlsCertPath, tlsKeyPath)
	if err != nil {
		return nil, err
	}

	caCert, err := os.ReadFile(tlsCaCertPath)
	if err != nil {
		return nil, err
	}

	caCertPool, err := x509.SystemCertPool()
	if err != nil {
		caCertPool = x509.NewCertPool()
	}
	if caCert != nil {
		caCertPool.AppendCertsFromPEM(caCert)
	}
	return &tls.Config{
		Certificates: []tls.Certificate{certificate},
		RootCAs:      caCertPool,
	}, nil
}
