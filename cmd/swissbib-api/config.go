package main

import (
	ublogger "gitlab.switch.ch/ub-unibas/go-ublogger"
	"log"
	"os"
	"strconv"
	"strings"
)

func loadEnv(envVar string, defaultValue interface{}, hasDefaultValue bool) string {
	val, exists := os.LookupEnv(envVar)
	if !exists {
		if hasDefaultValue {
			return defaultValue.(string)
		}
		log.Fatalf("Could not find environment variable: %s", envVar)
	}
	return val
}

func LoadBool(envVar string, defaultValue bool, hasDefaultValue bool) bool {
	val := loadEnv(envVar, strconv.FormatBool(defaultValue), hasDefaultValue)
	return strings.ToLower(val) == "true"
}

func LoadInt(envVar string, defaultValue int, hasDefaultValue bool) int {
	val := loadEnv(envVar, strconv.Itoa(defaultValue), hasDefaultValue)
	num, err := strconv.Atoi(val)
	if err != nil {
		log.Fatalf("Could not parse integer from string for setting: %s", envVar)
	}
	return num
}

func LoadString(envVar, defaultValue string, hasDefaultValue bool) string {
	return loadEnv(envVar, defaultValue, hasDefaultValue)
}

type ServiceConfig struct {
	MongoDbHost                   string
	MongoDbPort                   string
	MongoDbAuthSrc                string
	MongoDbUsername               string
	MongoDbPassword               string
	MongoDbDatabase               string
	MongoDbCollection             string
	MongoDbTlsCaCertPath          string
	MongoDbTlsCombinedCertificate string

	HttpAddress string

	LogFile           string
	LogLevel          string
	LogFormat         string
	LogToLogstash     bool
	LogstashHost      string
	LogstashPort      int
	LogstashNamespace string
	LogstashDataset   string
	LogstashLogLevel  string
	LogstashCert      string
	LogstashCertKey   string
	LogstashCertCa    string
}

func LoadConfig() (*ServiceConfig, error) {
	defaultConf := &ServiceConfig{
		MongoDbHost:                   LoadString("MONGODB_HOST", "localhost", true),
		MongoDbPort:                   LoadString("MONGODB_PORT", "29017", true),
		MongoDbAuthSrc:                LoadString("MONGODB_CREDENTIALS_DATABASE", "swissbib-legacy", true),
		MongoDbUsername:               LoadString("MONGODB_USERNAME", "", false),
		MongoDbPassword:               LoadString("MONGODB_PASSWORD", "", false),
		MongoDbDatabase:               LoadString("MONGODB_DATABASE", "swissbib-legacy", true),
		MongoDbCollection:             LoadString("MONGODB_COLLECTION", "concordance", true),
		MongoDbTlsCaCertPath:          LoadString("MONGODB_CA_CERT", "ssl/ca.crt", true),
		MongoDbTlsCombinedCertificate: LoadString("MONGODB_COMBINED_CERTIFICATE_PATH", "ssl/tls-combined.pem", true),
		HttpAddress:                   LoadString("SERVICE_HTTP_ADDRESS", "0.0.0.0:5000", true),
		LogFile:                       LoadString("SERVICE_LOG_FILE", "", true),
		LogLevel:                      LoadString("SERVICE_LOG_LEVEL", "info", true),
		LogFormat:                     LoadString("SERVICE_LOG_FORMAT", "%{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message}", true),
		LogToLogstash:                 LoadBool("SERVICE_LOG_TO_LOGSTASH", true, true),
		LogstashHost:                  LoadString("SERVICE_LOGSTASH_HOST", "localhost", true),
		LogstashPort:                  LoadInt("SERVICE_LOGSTASH_PORT", 5046, true),
		LogstashNamespace:             LoadString("SERVICE_LOGSTASH_NAMESPACE", "redirect-api", true),
		LogstashDataset:               LoadString("SERVICE_LOGSTASH_DATASET", "swissbib", true),
		LogstashLogLevel:              LoadString("SERVICE_LOGSTASH_LOG_LEVEL", "info", true),
		LogstashCert:                  LoadString("SERVICE_LOGSTASH_CERT", "ssl/tls.crt", true),
		LogstashCertKey:               LoadString("SERVICE_LOGSTASH_CERT_KEY", "ssl/tls.key", true),
		LogstashCertCa:                LoadString("SERVICE_LOGSTASH_CERT_CA", "ssl/ca.crt", true),
	}

	return defaultConf, nil
}

func (c ServiceConfig) DisplayConfigLog(logger *ublogger.Logger) {
	logger.Info().Msg("Service Configuration:")
	logger.Info().Msgf("MongoDB Host: %s", c.MongoDbHost)
	logger.Info().Msgf("MongoDB Port: %s", c.MongoDbPort)
	logger.Info().Msgf("MongoDB Auth Source: %s", c.MongoDbAuthSrc)
	logger.Info().Msgf("MongoDB Username: %s", c.MongoDbUsername)
	// logger.Info().Msgf("MongoDB Password: %s", c.MongoDbPassword)
	logger.Info().Msgf("MongoDB Database: %s", c.MongoDbDatabase)
	logger.Info().Msgf("MongoDB Collection: %s", c.MongoDbCollection)
	logger.Info().Msgf("MongoDB TlsCaCertPath: %s", c.MongoDbTlsCaCertPath)
	logger.Info().Msgf("MongoDB TlsCombinedCertificate: %s", c.MongoDbTlsCombinedCertificate)
	logger.Info().Msgf("HTTP Address: %s", c.HttpAddress)
	logger.Info().Msgf("Log File: %s", c.LogFile)
	logger.Info().Msgf("Log Level: %s", c.LogLevel)
	logger.Info().Msgf("Log Format: %s", c.LogFormat)
	logger.Info().Msgf("Log To Logstash: %t", c.LogToLogstash)
	logger.Info().Msgf("Logstash Host: %s", c.LogstashHost)
	logger.Info().Msgf("Logstash Port: %d", c.LogstashPort)
	logger.Info().Msgf("Logstash Namespace: %s", c.LogstashNamespace)
	logger.Info().Msgf("Logstash Log Level: %s", c.LogstashLogLevel)
	logger.Info().Msgf("Logstash Cert: %s", c.LogstashCert)
	logger.Info().Msgf("Logstash Cert Key: %s", c.LogstashCertKey)
	logger.Info().Msgf("Logstash Cert CA: %s", c.LogstashCertCa)
}
